﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.Windows;

namespace UI
{
       public class Staff2 : Students
    {
        /* Author: Ivaylo Bogdanov
       * Matriculation Number:40110832
       * Class Staff2 is used to create fields, properties and
       * Methods for Students created in GUI
       * Date last modified: 12/12/14
      */
        private int payroll_number;
        private string department;
        private string role;


        //Declare a PayrollNumber propery of type Int
        public int PayrollNumber
        {
            get
            {
                return payroll_number;
            }
            set
            {
                payroll_number = value;
            }


        }
        //Declare a Department propery of type String
        public string Department
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
            }
        }
        //Declare a Role propery of type String
        public string Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }

        public static void AddStaff(int payroll_number, string first_n, string second_n, string city, string street, string post_code, string email, string department, string role)
        {

            /* This method is used to take values from boxes in UI
             * and insert it into Properties which are afterwards
             * inserted into Table Staff. This methods also
             * opens new sql connection to make possible inseting 
             * data into Student Table. After susseccful inserting, connection
             * is closed, message box pops out saying saved.
             */

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            try
            {
                string sql = "Insert into Staff (PayrollNumber,Fname,Lname,City,Street,PostCode,Email,Department,Role) values(" + payroll_number
                                                                                                                            + ",'" + first_n
                                                                                                                            + "','" + second_n
                                                                                                                            + "','" + city
                                                                                                                            + "','" + street
                                                                                                                            + "','" + post_code
                                                                                                                            + "','" + email
                                                                                                                            + "','" + department
                                                                                                                            + "','" + role + "')";
                SqlCommand newconn = new SqlCommand(sql, con);
                con.Open();
                newconn.ExecuteNonQuery();
                string query = "select * from Staff";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();

                MessageBox.Show("Saved");


            }
        }


    }
}
