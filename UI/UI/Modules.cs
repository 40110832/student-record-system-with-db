﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
   public class Modules  
    {
        /* Author: Ivaylo Bogdanov
        * Matriculation Number:40110832
        * Class Modules is used to create fields, properties and
        * Methods for Modules created in GUI
        * Date last modified: 12/12/14
        */

        private string modulecode;
        private string modleader_firstname;
        private string modleader_lastname;
        private string module_name;


        //Declare a Modulecode propery of type String
        public string Modulecode
        {
            get
            {
                return modulecode;
            }
            set
            {
                modulecode = value;
            }
        }
        //Declare a ModleaderFirstname propery of type String
        public string ModleaderFirstname
        {
            get
            {
                return modleader_firstname;
            }
            set
            {
                modleader_firstname = value;
            }
        }

        //Declare a SecondName propery of type String
        public string ModleaderLastname
        {
            get
            {
                return modleader_lastname;
            }
            set
            {
                modleader_lastname = value;
            }
        }
        //Declare a City propery of type String
        public string ModuleName
        {
            get
            {
                return module_name;
            }
            set
            {
                module_name = value;
            }
        }
    }
}