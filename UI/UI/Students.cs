﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.Windows;

namespace UI
{
   public class Students
    {
        /* Author: Ivaylo Bogdanov
        * Matriculation Number:40110832
        * Class Students is used to create fields and properties and
        * Methods for Students created in GUI
        * Date last modified: 12/12/14
       */
        public int matric_number;
        private string first_n;
        private string second_n;
        private string city;
        private string street;
        private string post_code;
        private string email;
        private int mark;


        //Declare a MatricNumber propery of type Int
        public int MatricNumber
        {
            get
            {
                return matric_number;
            }
            set
            {
                matric_number = value;
            }
        }
        //Declare a FirstName propery of type String
        public string FirstName
        {
            get
            {
                return first_n;
            }
            set
            {
                first_n = value;
            }
        }

        //Declare a SecondName propery of type String
        public string SecondName
        {
            get
            {
                return second_n;
            }
            set
            {
                second_n = value;
            }
        }
        //Declare a City propery of type String
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        //Declare a Street propery of type String
        public string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }
        //Declare a PC propery of type String
        public string PC  //Postcode
        {
            get
            {
                return post_code;
            }
            set
            {
                post_code = value;
            }
        }
        //Declare a Email propery of type String
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        //Declare a Mark propery of type Int
        public int Mark
        {
            get
            {
                return mark;
            }
            set
            {
                mark = value;
            }
        }

        public static void Addstudent(int matric_number, string first_n, string second_n, string city, string street, string post_code, string email)
        {

            /* This method is used to take values from boxes in UI
             * and insert it into Properties which are afterwards
             * inserted into Table Students. This methods also
             * opens new sql connection to make possible inseting 
             * data into Student Table. After susseccful inserting, connection
             * is closed, message box pops out saying saved.
             */

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            try
            {
                con.Open();
                string sql = "Insert into STUDENTS (MatricNumber,FirstName,LastName,City,StreetName,PostCode,Email) values(" + matric_number + ",'" + first_n + "','" + second_n + "','" + city + "','" + street + "','" + post_code + "','" + email + "')";
                SqlCommand newconn = new SqlCommand(sql, con);
                newconn.ExecuteNonQuery();
                string query = "select * from STUDENTS";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);



            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
                MessageBox.Show("Saved");



            }

        }


    }
}


