﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /* Author: Ivaylo Bogdanov
         * Matriculation Number:40110832
         * Creates the Mainwindow
         * declaring objects - staff1,stud1 and mod1
         * assigning the values of the properites
         * using Validation 
         * using Visibility
         * Date last modified: 12/12/14
         */

        Staff2 staff1 = new Staff2();
        Students stud1 = new Students();
        Modules mod1 = new Modules();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void students_Click(object sender, RoutedEventArgs e)
        {
            // making Student grid visible when Students button is pressed and hide all other grids
            staffDataGrid.Visibility = System.Windows.Visibility.Hidden;
            modulesDataGrid.Visibility = System.Windows.Visibility.Hidden;
            grdPatient1.Visibility = System.Windows.Visibility.Visible;
            enrollmentDataGrid.Visibility = System.Windows.Visibility.Hidden;
            GridDesign.Visibility = System.Windows.Visibility.Hidden;
        }


        private void staff_Click_1(object sender, RoutedEventArgs e)
        {
            // making Staff grid visible when Staff button is pressed and hide all other grids
            staffDataGrid.Visibility = System.Windows.Visibility.Visible;
            modulesDataGrid.Visibility = System.Windows.Visibility.Hidden;
            grdPatient1.Visibility = System.Windows.Visibility.Hidden;
            enrollmentDataGrid.Visibility = System.Windows.Visibility.Hidden;
            GridDesign.Visibility = System.Windows.Visibility.Hidden;
        }

        private void modules_Click(object sender, RoutedEventArgs e)
        {
            // making modules grid visible when module button is pressed and hide all other grids
            staffDataGrid.Visibility = System.Windows.Visibility.Hidden;
            modulesDataGrid.Visibility = System.Windows.Visibility.Visible;
            grdPatient1.Visibility = System.Windows.Visibility.Hidden;
            enrollmentDataGrid.Visibility = System.Windows.Visibility.Hidden;
            GridDesign.Visibility = System.Windows.Visibility.Hidden;
        }

        private void enrolled_students_Click(object sender, RoutedEventArgs e)
        {
            // making Enrolled Students grid visible when Enrolled Students button is pressed and hide all other grids
            
            staffDataGrid.Visibility = System.Windows.Visibility.Hidden;
            modulesDataGrid.Visibility = System.Windows.Visibility.Hidden;
            grdPatient1.Visibility = System.Windows.Visibility.Hidden;
            enrollmentDataGrid.Visibility = System.Windows.Visibility.Visible;
            GridDesign.Visibility = System.Windows.Visibility.Hidden;
        }

        private void studentadd_Click(object sender, RoutedEventArgs e)
        {
            // when Add Student is clicked, this method shows Save button, Student labels and Textboxes
            // related to Student
            grdPatient1.Visibility = System.Windows.Visibility.Visible;
            gridStudents.Visibility = System.Windows.Visibility.Visible;
            savestud1.Visibility = System.Windows.Visibility.Visible;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            enrollstud.Visibility = System.Windows.Visibility.Hidden;
            enrollgrid.Visibility = System.Windows.Visibility.Hidden;
            Update.Visibility = System.Windows.Visibility.Hidden;
            gridMark.Visibility = System.Windows.Visibility.Hidden;
            delete.Visibility = System.Windows.Visibility.Hidden;
            deletegrid.Visibility = System.Windows.Visibility.Hidden;
            changeml.Visibility = System.Windows.Visibility.Hidden;
            reassigngrid.Visibility = System.Windows.Visibility.Hidden;
         
        }


        private void savestud1_Click(object sender, RoutedEventArgs e)
        {
            //validation
            

            if (matricBox.Text == "" || Convert.ToInt32(matricBox.Text) < 1000 || Convert.ToInt32(matricBox.Text) > 9000 || first_Name.Text == "" || last_Name.Text == "" ||
                cityBox.Text == "" || street_Name.Text == "" || post_Code.Text == "" || email_Text.Text == "" || email_Text.Text.Contains("@") == false)
            {
                //if something is true, following messagebox will appear
                MessageBox.Show("Please fill all empty fields before clicking this button!Also check that matric number is between 1000 and 9000 and you have enetered valid Email)");
            }
            else
            {   
                // using Addstudent and RefreshStud methods from Student.cs and Methods.cs
                // clears boxes
               
                stud1.MatricNumber = Convert.ToInt32(matricBox.Text);
                stud1.FirstName = first_Name.Text;
                stud1.SecondName = last_Name.Text;
                stud1.City = cityBox.Text;
                stud1.Street = street_Name.Text;
                stud1.PC = post_Code.Text;
                stud1.Email = email_Text.Text;
                Students.Addstudent(stud1.MatricNumber, stud1.FirstName, stud1.SecondName, stud1.City, stud1.Street, stud1.PC, stud1.Email);
                Methods.RefreshStud(grdPatient1);

                //changing visibility and clears boxes
                grdPatient1.UpdateLayout();
                GridDesign.Visibility = System.Windows.Visibility.Hidden;
                savestud1.Visibility = System.Windows.Visibility.Hidden;
                gridStudents.Visibility = System.Windows.Visibility.Hidden;
                matricBox.Clear();
                first_Name.Clear();
                last_Name.Clear();
                cityBox.Clear();
                street_Name.Clear();
                post_Code.Clear();
                email_Text.Clear();
                paynm.Clear();
                
            }


        }

        private void staffadd_Click(object sender, RoutedEventArgs e)
        {
            // when Add Staff is clicked, this method shows Save button, Staff labels and Textboxes
            // related to Staff

            gridStudents.Visibility = System.Windows.Visibility.Hidden;
            savestud1.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Visible;
            gridstaff.Visibility = System.Windows.Visibility.Visible;
            enrollstud.Visibility = System.Windows.Visibility.Hidden;
            enrollgrid.Visibility = System.Windows.Visibility.Hidden;
            Update.Visibility = System.Windows.Visibility.Hidden;
            gridMark.Visibility = System.Windows.Visibility.Hidden;
            delete.Visibility = System.Windows.Visibility.Hidden;
            deletegrid.Visibility = System.Windows.Visibility.Hidden;
            changeml.Visibility = System.Windows.Visibility.Hidden;
            reassigngrid.Visibility = System.Windows.Visibility.Hidden;
        }

        private void savestaf1_Click(object sender, RoutedEventArgs e)
        {
            //validation
            if (pn.Text == "" || Convert.ToInt32(pn.Text) < 9000 || Convert.ToInt32(pn.Text) > 9999 || first_namest.Text == "" || last_namest.Text == "" || cityBox1.Text == "" ||
                street_Name1.Text == "" || post_Code1.Text == "" || email_Text1.Text == "" || email_Text1.Text.Contains("@") == false || dep.Text == "" || role.Text == "")
            {
                 MessageBox.Show("Please fill all empty fields before clicking this button!Also check that Payroll number is between 9000 and 9999 and you have enetered valid Email)");
            }

            else{

             // using AddStafft and RefreshStaff methods from Staff2.cs and Methods.cs
             // clears boxes

            staffDataGrid.Visibility = System.Windows.Visibility.Visible;  
            staff1.PayrollNumber = Convert.ToInt32(pn.Text);
            staff1.FirstName = first_namest.Text;
            staff1.SecondName = last_namest.Text;
            staff1.City = cityBox1.Text;
            staff1.Street = street_Name1.Text;
            staff1.PC = post_Code1.Text;
            staff1.Email = email_Text1.Text;
            staff1.Department = dep.Text;
            staff1.Role = role.Text;
            Staff2.AddStaff(staff1.PayrollNumber, staff1.FirstName, staff1.SecondName, staff1.City, staff1.Street, staff1.PC, staff1.Email, staff1.Department, staff1.Role);
            Methods.RefreshStaff(staffDataGrid);
            GridDesign.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            pn.Clear();
            first_namest.Clear();
            last_namest.Clear();
            cityBox1.Clear();
            street_Name1.Clear();
            post_Code1.Clear();
            email_Text1.Clear();
            dep.Clear();
        
            }
        }

        private void studenroll_Click(object sender, RoutedEventArgs e)
        {

            // when Enroll Student is clicked, this method shows enroll button, studentenroll labels and Textboxes
          
            gridStudents.Visibility = System.Windows.Visibility.Hidden;
            savestud1.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            enrollstud.Visibility = System.Windows.Visibility.Visible;
            enrollgrid.Visibility = System.Windows.Visibility.Visible;
            Update.Visibility = System.Windows.Visibility.Hidden;
            gridMark.Visibility = System.Windows.Visibility.Hidden;
            delete.Visibility = System.Windows.Visibility.Hidden;
            deletegrid.Visibility = System.Windows.Visibility.Hidden;
            changeml.Visibility = System.Windows.Visibility.Hidden;
            reassigngrid.Visibility = System.Windows.Visibility.Hidden;
        }

        private void enrollstud_Click(object sender, RoutedEventArgs e)
        {
            // validation
            if (mntbox.Text == "" || Convert.ToInt32(mntbox.Text) < 1000 || Convert.ToInt32(mntbox.Text) > 9000 || mcbox.Text == "")
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!Also check that matric number is between 1000 and 9000!)");
            }

            else
            {
                // using EnrollStud methods and and Refreshenroll method
                // hides enrollgrid and enrollstud

                stud1.MatricNumber = Convert.ToInt32(mntbox.Text);
                mod1.Modulecode = mcbox.Text;
                Methods.EnrollStud(stud1, mod1);
                Methods.RefreshEnroll(enrollmentDataGrid);
                enrollgrid.Visibility = System.Windows.Visibility.Hidden;
                enrollstud.Visibility = System.Windows.Visibility.Hidden;
                mntbox.Clear();
            }
        }

        private void Upd_mark_Click(object sender, RoutedEventArgs e)
        {
            // when Update Mark is clicked, this method shows Save button, Update grid and Textboxes

            gridStudents.Visibility = System.Windows.Visibility.Hidden;
            savestud1.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            enrollstud.Visibility = System.Windows.Visibility.Hidden;
            enrollgrid.Visibility = System.Windows.Visibility.Hidden;
            Update.Visibility = System.Windows.Visibility.Visible;
            gridMark.Visibility = System.Windows.Visibility.Visible;
            delete.Visibility = System.Windows.Visibility.Hidden;
            deletegrid.Visibility = System.Windows.Visibility.Hidden;
            changeml.Visibility = System.Windows.Visibility.Hidden;
            reassigngrid.Visibility = System.Windows.Visibility.Hidden;


        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            //validation
            
            if (mnbox2.Text == "" || Convert.ToInt32(mnbox2.Text) < 1000 || Convert.ToInt32(mnbox2.Text) > 9000 || mcbox2.Text == "" || markbox.Text == "" || Convert.ToInt32(markbox.Text) <=0 || Convert.ToInt32(markbox.Text) > 100)
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!Also check that matric number is between 1000 and 9000 and you have enetered Mark between 0 and 100)");
            }
            else
            {
                // using UpdateMark methods and and RefreshEnroll method
                // hides GridDesign, Update and gridMark

                enrollgrid.Visibility = System.Windows.Visibility.Hidden;
                stud1.MatricNumber = Convert.ToInt32(mnbox2.Text);
                mod1.Modulecode = mcbox2.Text;
                stud1.Mark = Convert.ToInt32(markbox.Text);
                Methods.UpdateMark(stud1, mod1);
                Methods.RefreshEnroll(enrollmentDataGrid);
                GridDesign.Visibility = System.Windows.Visibility.Hidden;
                Update.Visibility = System.Windows.Visibility.Hidden;
                gridMark.Visibility = System.Windows.Visibility.Hidden;

                mnbox2.Clear();
                markbox.Clear();
            }

        }

        private void studentdel_Click(object sender, RoutedEventArgs e)
        {
            // when Delete button is clicked, this method shows Save button, Delete grid and Textboxes

            gridStudents.Visibility = System.Windows.Visibility.Hidden;
            savestud1.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            enrollstud.Visibility = System.Windows.Visibility.Hidden;
            enrollgrid.Visibility = System.Windows.Visibility.Hidden;
            Update.Visibility = System.Windows.Visibility.Hidden;
            gridMark.Visibility = System.Windows.Visibility.Hidden;
            delete.Visibility = System.Windows.Visibility.Visible;
            deletegrid.Visibility = System.Windows.Visibility.Visible;
            changeml.Visibility = System.Windows.Visibility.Hidden;
            reassigngrid.Visibility = System.Windows.Visibility.Hidden;
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {

            //validation
            if (mn.Text == "" || Convert.ToInt32(mn.Text) < 1000 || Convert.ToInt32(mn.Text) > 9000 || mc.Text == "")
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!Also check that matric number is between 1000 and 9000)");
            }
            else
            {
                // using Delete methods and and RefreshEnroll method
                // hides GridDesign, delete and deletegrid

                deletegrid.Visibility = System.Windows.Visibility.Visible;
                stud1.MatricNumber = Convert.ToInt32(mn.Text);
                mod1.Modulecode = mc.Text;
                Methods.Delete(stud1, mod1);
                Methods.RefreshEnroll(enrollmentDataGrid);
                GridDesign.Visibility = System.Windows.Visibility.Hidden;
                delete.Visibility = System.Windows.Visibility.Hidden;
                deletegrid.Visibility = System.Windows.Visibility.Hidden;
                mn.Clear();
             
            }
        }

        private void modleadreassign_Click(object sender, RoutedEventArgs e)
        {
            // when Reassign Module Leader button is clicked, this method shows Save button, reassign grid and Textboxes

            gridStudents.Visibility = System.Windows.Visibility.Hidden;
            savestud1.Visibility = System.Windows.Visibility.Hidden;
            savestaf1.Visibility = System.Windows.Visibility.Hidden;
            gridstaff.Visibility = System.Windows.Visibility.Hidden;
            enrollstud.Visibility = System.Windows.Visibility.Hidden;
            enrollgrid.Visibility = System.Windows.Visibility.Hidden;
            Update.Visibility = System.Windows.Visibility.Hidden;
            gridMark.Visibility = System.Windows.Visibility.Hidden;
            delete.Visibility = System.Windows.Visibility.Hidden;
            deletegrid.Visibility = System.Windows.Visibility.Hidden;
            changeml.Visibility = System.Windows.Visibility.Visible;
            reassigngrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void changeml_Click(object sender, RoutedEventArgs e)
        {
            //validation
            if (paynm.Text == "" || Convert.ToInt32(paynm.Text) < 9000 || Convert.ToInt32(paynm.Text) > 9999 || modcd.Text == "")
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!Also check that payroll number is between 9000 and 9999)");
            }
            else
            {
                // using Reassign methods and and RefreshModule method
                // hides GridDesign, changeml and reassigngrid

                modulesDataGrid.Visibility = System.Windows.Visibility.Visible;
                staff1.PayrollNumber = Convert.ToInt32(paynm.Text);
                mod1.Modulecode = modcd.Text;
                Methods.Reassign(staff1, mod1);
                Methods.RefreshModule(modulesDataGrid);
                GridDesign.Visibility = System.Windows.Visibility.Hidden;
                changeml.Visibility = System.Windows.Visibility.Hidden;
                reassigngrid.Visibility = System.Windows.Visibility.Hidden;

                paynm.Clear();
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            UI.NEWDatabaseDataSet nEWDatabaseDataSet = ((UI.NEWDatabaseDataSet)(this.FindResource("nEWDatabaseDataSet")));
            // Load data into the table Students. 
            UI.NEWDatabaseDataSetTableAdapters.StudentsTableAdapter nEWDatabaseDataSetStudentsTableAdapter = new UI.NEWDatabaseDataSetTableAdapters.StudentsTableAdapter();
            nEWDatabaseDataSetStudentsTableAdapter.Fill(nEWDatabaseDataSet.Students);
            System.Windows.Data.CollectionViewSource studentsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("studentsViewSource")));
            studentsViewSource.View.MoveCurrentToFirst();

            // Load data into the table Modules. 
            UI.NEWDatabaseDataSetTableAdapters.ModulesTableAdapter nEWDatabaseDataSetModulesTableAdapter = new UI.NEWDatabaseDataSetTableAdapters.ModulesTableAdapter();
            nEWDatabaseDataSetModulesTableAdapter.Fill(nEWDatabaseDataSet.Modules);
            System.Windows.Data.CollectionViewSource modulesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("modulesViewSource")));
            modulesViewSource.View.MoveCurrentToFirst();
            // Load data into the table Staff. 
            UI.NEWDatabaseDataSetTableAdapters.StaffTableAdapter nEWDatabaseDataSetStaffTableAdapter = new UI.NEWDatabaseDataSetTableAdapters.StaffTableAdapter();
            nEWDatabaseDataSetStaffTableAdapter.Fill(nEWDatabaseDataSet.Staff);
            System.Windows.Data.CollectionViewSource staffViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("staffViewSource")));
            staffViewSource.View.MoveCurrentToFirst();
            // Load data into the table Enrollment.
            UI.NEWDatabaseDataSetTableAdapters.EnrollmentTableAdapter nEWDatabaseDataSetEnrollmentTableAdapter = new UI.NEWDatabaseDataSetTableAdapters.EnrollmentTableAdapter();
            nEWDatabaseDataSetEnrollmentTableAdapter.Fill(nEWDatabaseDataSet.Enrollment);
            System.Windows.Data.CollectionViewSource enrollmentViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("enrollmentViewSource")));
            enrollmentViewSource.View.MoveCurrentToFirst();

            //clear all boxes
            matricBox.Clear();
            first_Name.Clear();
            last_Name.Clear();
            cityBox.Clear();
            street_Name.Clear();
            post_Code.Clear();
            email_Text.Clear();
            pn.Clear();
            first_namest.Clear();
            last_namest.Clear();
            cityBox1.Clear();
            street_Name1.Clear();
            post_Code1.Clear();
            email_Text1.Clear();
            dep.Clear();
            mntbox.Clear();
            mnbox2.Clear();
            markbox.Clear();
            mn.Clear();
            paynm.Clear();

        }
    }
}




