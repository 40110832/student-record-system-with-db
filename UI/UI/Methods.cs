﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.Windows;

namespace UI
{
  public class Methods
    {
        /* Author: Ivaylo Bogdanov
         * Matriculation Number:40110832
         * Class Methods is used to create all not built-in Methods in separate class
         * Date last modified: 12/12/14
        */

         

         
        public static void EnrollStud(Students student, Modules modulecode)
        {

            /* This method is used to take values from boxes in UI
            * and insert it into Properties which are afterwards
            * inserted into Table Enrollment. This methods also
            * opens new sql connection to make possible inseting 
            * data into Student Enrollment. After susseccful inserting, connection
            * is closed, message box pops out saying saved.
            */
           

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            try
            {
                string sql = "Insert into Enrollment (MatriculationNumber,ModuleCode) values(" + student.matric_number  + ",'" + modulecode.Modulecode + "')";
                SqlCommand newconn = new SqlCommand(sql, con);
                con.Open();
                newconn.ExecuteNonQuery();
                string query = "select * from Enrollment";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
           


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
                MessageBox.Show("Saved");
                

            }
        }

        public static void UpdateMark(Students mnumber2, Modules mcode2)
        {

            /* This method is also similar to the previous method.
             * This time  update statement is used in order to 
             *  update mark in Enrollment table
             *  method also used if and else stament to test Mark 
             *  and change Status in datagrid
             */
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            try
            {
                string sql;
                if (mnumber2.Mark <= 39)
                {
                 sql = @"UPDATE Enrollment SET Mark=" + mnumber2.Mark +",Status = 'Failed' WHERE MatriculationNumber=" + mnumber2.MatricNumber + " AND (ModuleCode= '" + mcode2.Modulecode + "')";
                 
                }
                else
                {
                  sql = @"UPDATE Enrollment SET Mark=" + mnumber2.Mark +", Status = 'Pass' WHERE MatriculationNumber=" + mnumber2.MatricNumber + " AND (ModuleCode= '" + mcode2.Modulecode + "')";
                }
                SqlCommand newconn = new SqlCommand(sql, con);
                con.Open();
                newconn.ExecuteNonQuery();
                string query = "select * from Enrollment";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
              


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
                MessageBox.Show("Saved");
            

            }
        }

         
          public static void Delete(Students matric, Modules modulecode)
          {
              /* This Method creates new sql connection and removes column
               * The method looks Matric number and Modulecode and inserts them into tavle Enrollment
               */
             SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            try
            {              
                string sql = "delete from Enrollment where MatriculationNumber=" + matric.MatricNumber + " AND ModuleCode='" + modulecode.Modulecode+"'";
                SqlCommand newconn = new SqlCommand(sql, con2);
                con2.Open();
                newconn.ExecuteNonQuery();
                string query = "select * from Enrollment";
                SqlCommand cmd = new SqlCommand(query, con2);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
               

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con2.Close();
                MessageBox.Show("Saved");
              
            }
          }

          public static void Reassign(Staff2 payrlnumber, Modules modulecode)
          {
              /* Reassign  method is deleting First and Lastname of Database
               * Method takes payroll number and modulecode
               * to complete this function
               */
              SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
              try
              {
                  string sql = "Update Modules SET Modules.ModLeaderFname = Staff.Fname, Modules.ModLeaderLname = Staff.Lname FROM Staff Where Staff.Payrollnumber=" + payrlnumber.PayrollNumber + "AND Modules.ModuleCode='" + modulecode.Modulecode + "'";
                  SqlCommand newconn = new SqlCommand(sql, con2);
                  con2.Open();
                  newconn.ExecuteNonQuery();
                  string query = "select * from Modules";
                  SqlCommand cmd = new SqlCommand(query, con2);
                  SqlDataAdapter da = new SqlDataAdapter(cmd);
                  DataTable dt = new DataTable();
                  da.Fill(dt);
             


              }

              catch (Exception ex)
              {
                  MessageBox.Show(ex.Message);
              }
              finally
              {
                  con2.Close();
                  MessageBox.Show("Saved");
                 
              }
          }

           public static void RefreshStud(DataGrid datagrid)
          {
               //Refreshing Student datagrid and filling it with data, used for instant  refresh
                           
                  SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
                  con2.Open();
                  string query = string.Format("select * from Students");
                  SqlCommand cmd = new SqlCommand(query, con2);
                  SqlDataAdapter da = new SqlDataAdapter(cmd);
                  DataTable dt = new DataTable();
                  da.Fill(dt);
                  datagrid.ItemsSource = dt.DefaultView;

              }

           public static void RefreshStaff(DataGrid datagrid)
           {
              //Refreshing Staff datagrid and filling it with data, used for instant refresh
              //creates new connection for this purpose

               SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
               con2.Open();
               string query = string.Format("select * from Staff");
               SqlCommand cmd = new SqlCommand(query, con2);
               SqlDataAdapter da = new SqlDataAdapter(cmd);
               DataTable dt = new DataTable();
               da.Fill(dt);
               datagrid.ItemsSource = dt.DefaultView;

           }

           public static void RefreshEnroll(DataGrid datagrid)
           {
               //Refreshing Enroll datagrid and filling it with data, used for instant refresh

               SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
               con2.Open();
               string query = string.Format("select * from Enrollment");
               SqlCommand cmd = new SqlCommand(query, con2);
               SqlDataAdapter da = new SqlDataAdapter(cmd);
               DataTable dt = new DataTable();
               da.Fill(dt);
               datagrid.ItemsSource = dt.DefaultView;

           }

           public static void RefreshModule(DataGrid datagrid)
           {
               //Refreshing Module datagrid and filling it with data, used for instant refresh

               SqlConnection con2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\assessment2\UI\UI\NEWDatabase.mdf;Integrated Security=True;Connect Timeout=30");
               con2.Open();
               string query = string.Format("select * from Modules");
               SqlCommand cmd = new SqlCommand(query, con2);
               SqlDataAdapter da = new SqlDataAdapter(cmd);
               DataTable dt = new DataTable();
               da.Fill(dt);
               datagrid.ItemsSource = dt.DefaultView;

           }              
  }
    }


      




    

