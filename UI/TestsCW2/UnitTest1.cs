﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UI;


namespace TestsCW2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public static void newTest()
        {
            Students student = new Students();
            student.MatricNumber = 1001;
            student.FirstName = "Ivaylo";
            student.SecondName = "Bogdanov";
            student.City = "Edinburgh";
            student.PC = "EH10 5AG";
            student.Street = "Mardale Crescent 6";
            student.Email = "ivailo.bog@gmail.com";
            Methods.Addstudent(student.MatricNumber, student.FirstName, student.SecondName, student.City, student.PC, student.Street, student.Email);
        }
    }
}
