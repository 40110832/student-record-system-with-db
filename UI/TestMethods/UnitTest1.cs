﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMethods
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestStudents()
        {
            //Testing Addstudent
            Students student = new Students();
            student.MatricNumber = 1816;
            student.FirstName = "";
            student.SecondName = "Bogdanov";
            student.City = "Edinburgh";
            student.PC = "EH10 5AG";
            student.Street = "Mardale Crescent 6";
            student.Email = "ivailo.bog@gmail.com";
            Students.Addstudent(student.MatricNumber, student.FirstName, student.SecondName, student.City, student.PC, student.Street, student.Email);
        }

        [TestMethod]
        public void TestStaff()
        {
            //Testing Addstaff
            Staff2 staff = new Staff2();
            staff.PayrollNumber = 9666;
            staff.FirstName = "Andrew";
            staff.SecondName = "Cumming";
            staff.City = "Edinburgh";
            staff.PC = "new";
            staff.Street = "new2";
            staff.Email = "new3" ;
            staff.Department = "Computing";
            staff.Role = "Lecturer";
            Staff2.AddStaff(staff.PayrollNumber, staff.FirstName, staff.SecondName, staff.City, staff.PC, staff.Street, staff.Email, staff.Department, staff.Role);
        }      
    }
}
